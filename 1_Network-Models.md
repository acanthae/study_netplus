# Network Models

## Objectives

- 1.2 Explain devices at OSI level
- 1.3 Explain concepts of routing and switching

## Overview

- Two method of conventionalizing networks:
  - Open Systems Interconnection seven-layer model (OSI)
  - Transmission Control Protocol//Internet Protocol (TCP/IP)

## The OSI Seven-Layer Model

- **Protocols** - sets of clearly defined standards
- Encourages modular design

### The Layers

- **7** - Application
- **6** - Presentation
- **5** - Session
- **4** - Transport
- **3** - Network
- **2** - Data Link
- **1** - Physical

**Please do not throw sausage pizza away.**

### Layer One - Physical

- 
