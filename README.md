# README

Study notes for the CompTIA Network+ Exam.

- **Study Start:** 2021/03/31

## References

- [CompTIA Network+ Certification All-in-One Exam Guide, Seventh Edition](https://www.amazon.com/CompTIA-Network-Certification-Seventh-N10-007/dp/1260122387/ref=sr_1_1?dchild=1&keywords=network%2B&qid=1617218594&sr=8-1)
